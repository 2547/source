/** @format */

import React, { Component } from 'react'
import {
  Animated,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  View,
  Text,
  Dimensions,
} from 'react-native'

import { connect } from 'react-redux'
import { Color, Tools, Config, Languages, AppConfig, Images } from '@common'
import { CategoryCarousel, AnimatedHeader } from '@components'
import Parallax from 'react-native-parallax'
import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome'
const size = Config.PostImage.large
class Categories extends Component {
  constructor(props) {
    super(props)
    this.page = 1
    this.state = {
      scrollY: new Animated.Value(0),
    }
    this.defaultCates = [
      { image: Images.imageBase, name: 'Category...', count: 'Updating...' },
      { image: Images.imageBase, name: 'Category...', count: 'Updating...' },
      { image: Images.imageBase, name: 'Category...', count: 'Updating...' },
    ]
  }

  componentDidMount() {
    this.props.fetchCategories()
  }

  componentWillReceiveProps(nextProps) {
    return (
      this.props.selectedLayout != nextProps.selectedLayout ||
      this.props.categories != nextProps.categories
    )
  }

  showCategory = (category) => {
    const { setActiveCategory, onViewCategory } = this.props
    setActiveCategory(category.id)
    onViewCategory(category)
  }

  changeLayout = () => this.props.setActiveLayout(!this.props.selectedLayout)

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    )
  }

  nextCategories = () => {
    this.page += 1
    this.props.fetchCategories(this.page)
  }

  _renderItem = ({ item, index }) => {
    const { categories } = this.props
    let category = item
    let cateImage
    if (category.count > 0) {
      if (
        category.image &&
        category.image.length > 0 &&
        category.image != null
      ) {
        // optimize image
        let temp = category.image[0].file
        temp = temp.substring(temp.indexOf('.'), 0)
        let tag = category.image[0].file.split('.').pop()
        cateImage = `${
          AppConfig.Website.url
        }/wp-content/uploads/${temp}-${size}.${tag}`
      }
      return (
        <Parallax.Image
          key={index}
          onPress={() => this.showCategory(category)}
          style={styles.image}
          overlayStyle={styles.overlay}
          containerStyle={[
            styles.containerStyle,
            index === categories.length - 1 && {
              marginBottom: 50,
            },
          ]}
          parallaxFactor={0.9}
          defaultSource={Images.imageHolder}
          source={{ uri: cateImage }}>
          <View style={styles.titleView}>
            <Text style={styles.title}>
              {Tools.getDescription(category.name, 200)}
            </Text>
            <Text numberOfLines={2} style={styles.count}>
              {category.count + ' ' + Languages.placeToVisit}
            </Text>
          </View>
        </Parallax.Image>
      )
    }
    return <View />
  }

  renderContent = () => {
    const {
      categories,
      isFetching,
      onViewCategory,
      selectedLayout,
    } = this.props

    if (isFetching) {
      return (
        <View style={[styles.horizontalLoading]}>
          <ActivityIndicator size="small" color="#00ff00" />
        </View>
      )
    }

    if (selectedLayout !== Config.CategoryListView) {
      return <CategoryCarousel onViewCategory={onViewCategory} />
    }
    const filterCategories =
      typeof categories != 'undefined' ? categories : this.defaultCates

    return (
      <Parallax.ScrollView
        style={styles.wrapListCate}
        onScroll={Animated.event([
          { nativeEvent: { contentOffset: { y: this.state.scrollY } } },
        ])}
        scrollViewComponent={FlatList}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => index.toString()}
        data={filterCategories}
        onEndReached={(e) => {
          //warn(e.distanceFromEnd);
          e.distanceFromEnd > 500 && this.nextCategories()
        }}
        onEndReachedThreshold={1}
      />
    )
  }

  renderLayoutButton = () => {
    const hitSlop = { top: 20, right: 20, left: 20, bottom: 20 }
    return (
      <TouchableOpacity
        style={styles.fab}
        onPress={this.changeLayout}
        activeOpacity={0}
        hitSlop={hitSlop}>
        <Icon.Button
          onPress={this.changeLayout}
          color={Color.backButton.text}
          iconStyle={{ backgroundColor: 'transparent', left: 5 }}
          borderRadius={50}
          backgroundColor={'transparent'}
          name={'exchange'}
          size={14}
        />
      </TouchableOpacity>
    )
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <AnimatedHeader
          isCategory
          scrollY={this.state.scrollY}
          label={Languages.category}
        />
        {this.renderContent()}
        {this.renderLayoutButton()}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  const categories = state.categories.list
  const selectedLayout = state.categories.selectedLayout
  const isFetching = state.categories.isFetching
  return { categories, selectedLayout, isFetching }
}

const mapDispatchToProps = (dispatch) => {
  const {
    fetchCategories,
    setActiveCategory,
    setActiveLayout,
  } = require('@redux/actions')
  return {
    fetchCategories: () => dispatch(fetchCategories()),
    setActiveCategory: (id) => dispatch(setActiveCategory(id)),
    setActiveLayout: (enable) => dispatch(setActiveLayout(enable)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Categories)
