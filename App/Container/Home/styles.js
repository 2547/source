/** @format */

import { StyleSheet, Dimensions } from 'react-native'

export default StyleSheet.create({
  scrollView: {
    paddingBottom: 30,
    backgroundColor: '#fff',
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  body: {
    backgroundColor: '#FFF',
    paddingTop: 40,
    flex: 1,
  },
})
