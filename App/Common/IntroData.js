/* This is a config for intro Data when start app */

export default [
  {
    title: 'Map Support',
    description:
      'Integrates map feature into your listing app to allow your end-users be able to find their nearest interested locations right from their mobile phones.',
    iconImage: require('images/map.png'),
  },
  {
    title: 'Push notifications',
    description:
      'Allow push notifications to get updated information and do not miss any important promotion or event from the app.',
    iconImage: require('images/ballon.png'),
  },
  {
    title: 'Calendar Event',
    description:
      'Booking new appointment or making reservations can be in just few minutes through Calendar Event Setting feature of ListApp.',
    iconImage: require('images/tree.png'),
  },

  {
    title: 'Offline Access',
    description:
      'You can save your location wish-lists and images offline for later use without the need of internet connection',
    iconImage: require('images/plan.png'),
  },
]
