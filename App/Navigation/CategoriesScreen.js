/** @format */

import React, { Component } from 'react'

import { withSafeArea } from 'react-native-safe-area-view'
import { Categories } from '@container'

@withSafeArea()
export default class CategoriesScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  render() {
    const { navigate } = this.props.navigation
    return (
      <Categories
        showBanner
        onViewPost={(post, index) => navigate('postDetail', { post, index })}
        onViewCategory={(item) => navigate('category', { mainCategory: item })}
      />
    )
  }
}
