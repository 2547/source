/** @format */

import React, { Component } from 'react'
import Theme from '@theme'
import { withSafeArea } from 'react-native-safe-area-view'

@withSafeArea()
export default class PostNewListingScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  render() {
    const { navigation } = this.props
    return (
      <Theme.PostNewListing
        onBack={() => navigation.goBack()}
        next={(post) => navigation.navigate('pickMap', { post })}
      />
    )
  }
}
