/** @format */

import React, { Component } from 'react'
import { PickMap } from '@container'
import { withSafeArea } from 'react-native-safe-area-view'

@withSafeArea()
export default class PickMapScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  render() {
    const { navigation } = this.props
    const { navigate } = navigation

    return (
      <PickMap
        navigation={navigation}
        onBack={() => navigation.goBack()}
        onClose={() => navigate('userProfile')}
        next={(post, pickMap) => navigate('postNewContent', { post, pickMap })}
      />
    )
  }
}
