/** @format */

import React, { Component } from 'react'

import { Languages, } from '@common'
import { Setting } from '@container'
import { Menu } from '@navigation/Icons'
import { withSafeArea } from 'react-native-safe-area-view'

@withSafeArea()
export default class SettingScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: Languages.setting,
    headerLeft: Menu()
  })

  render() {
    return (
        <Setting navigation={this.props.navigation}/>
    )
  }
}