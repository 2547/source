/** @format */

import React, { Component } from 'react'
import { ReadLater } from '@container'

import { connect } from 'react-redux'
import { withSafeArea } from 'react-native-safe-area-view'

@withSafeArea()
class ReadLaterScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  render = () => {
    const { user, navigation } = this.props
    const onViewPost = (item, index, parentPosts) =>
      navigation.navigate('postDetail', {
        post: item,
        index,
        parentPosts,
        backToRoute: 'userProfile',
      })

    return (
      <ReadLater
        onBack={() => navigation.goBack()}
        onLogIn={() => navigation.navigate('login')}
        userData={user.data}
        onViewPost={onViewPost}
      />
    )
  }
}
const mapStateToProps = ({ user }) => ({ user })
export default connect(mapStateToProps)(ReadLaterScreen)
