/** @format */

import React, { Component } from 'react'
import Theme from '@theme'
import { withSafeArea } from 'react-native-safe-area-view'

@withSafeArea()
export default class PostNewContentScreen extends Component {
  static navigationOptions = {
    header: null,
  }

  render() {
    const { navigation } = this.props
    const { navigate } = navigation

    return (
      <Theme.PostNewContent
        navigation={navigation}
        onBack={() => navigation.goBack()}
        onClose={() => navigate('userProfile')}
      />
    )
  }
}
