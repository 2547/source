/** @format */

import _AdFacebook from './AdFacebook'
export const AdFacebook = _AdFacebook

import _AdMob from './AdMob'
export const AdMob = _AdMob

import _Comment from './Comment'
export const Comment = _Comment

import _CommentIcons from './CommentIcons'
export const CommentIcons = _CommentIcons

import _TabBarIcon from './TabBarIcon'
export const TabBarIcon = _TabBarIcon

import _FacebookButton from './FacebookButton'
export const FacebookButton = _FacebookButton

import _FlatButton from './FlatButton'
export const FlatButton = _FlatButton

import _IconImage from './IconImage'
export const IconImage = _IconImage

import _CategoryBanner from './CategoryBanner'
export const CategoryBanner = _CategoryBanner

import _PostReadLater from './PostReadLater'
export const PostReadLater = _PostReadLater

import _PostLayout from './PostLayout'
export const PostLayout = _PostLayout

import _RelatedPost from './RelatedPost'
export const RelatedPost = _RelatedPost

import _ScrollTop from './ScrollTop'
export const ScrollTop = _ScrollTop

import _Slider from './Slider'
export const Slider = _Slider

import _Spinkit from './Spinkit'
export const Spinkit = _Spinkit

import _SwipeCards from './SwipeCards'
export const SwipeCards = _SwipeCards

import _Toolbar from './Toolbar'
export const Toolbar = _Toolbar

import _Video from './Video'
export const Video = _Video

import _WebView from './WebView'
export const WebView = _WebView

import _Button from './Button/Button'
export const Button = _Button

import _ModalBox from './Modal'
export const ModalBox = _ModalBox

import _ModalLayout from './Modal/Layout'
import _ModalTag from './Modal/Tag'
import _ModalCategory from './Modal/Category'
import _ModalFilter from './Modal/Filter'
import _ModalPhoto from './Modal/Photo'
import _ModalBooking from './Modal/Booking'
import _ModalComment from './Modal/Comment'
import _ModalChat from './Modal/Chat'

export const Modal = {
  Category: _ModalCategory,
  Layout: _ModalLayout,
  Tag: _ModalTag,
  Filter: _ModalFilter,
  Photo: _ModalPhoto,
  Booking: _ModalBooking,
  Comment: _ModalComment,
  Chat: _ModalChat,
}

import _TagSelect from './TagSelect'
export const TagSelect = _TagSelect

import _PostList from './PostList'
export const PostList = _PostList

import _CategoryList from './CategoryList'
export const CategoryList = _CategoryList

import _AnimatedHeader from './AnimatedHeader'
export const AnimatedHeader = _AnimatedHeader

import _AnimatedToolbar from './AnimatedToolbar'
export const AnimatedToolbar = _AnimatedToolbar

import _HeaderFilter from './HeaderFilter'
export const HeaderFilter = _HeaderFilter

import _LogoSpinner from './LogoSpinner'
export const LogoSpinner = _LogoSpinner

import _TabBar from './TabBar'
export const TabBar = _TabBar

import _Location from './Location'
export const Location = _Location

import _BrowseLocation from './BrowseLocation'
export const BrowseLocation = _BrowseLocation

import _Rating from './Rating'
export const Rating = _Rating

import _Reviews from './Reviews'
export const Reviews = _Reviews

import _Star from './Star'
export const Star = _Star

import _MapView from './MapView'
export const MapView = _MapView

import _Calendar from './Calendar'
export const Calendar = _Calendar

import _AgendaCalendar from './Calendar/Agenda'
export const AgendaCalendar = _AgendaCalendar

import _CarouselListing from './CarouselListing'
export const CarouselListing = _CarouselListing

import _DateTimePicker from './DateTimePicker'
export const DateTimePicker = _DateTimePicker

import _PickerPerson from './Picker'
export const PickerPerson = _PickerPerson

import _Mansory from './Mansory'
export const Mansory = _Mansory

import _HorizonList from './HorizonList'
export const HorizonList = _HorizonList

import _Board from './BoardGradient'
export const Board = _Board

import _ImageCache from './ImageCache'
export const ImageCache = _ImageCache

import _CategoryCarousel from './CategoryCarousel'
export const CategoryCarousel = _CategoryCarousel

import _SearchBar from './SearchBar'
export const SearchBar = _SearchBar

import _UserProfileItem from './UserProfileItem'
export const UserProfileItem = _UserProfileItem

import _UserProfileHeader from './UserProfileHeader'
export const UserProfileHeader = _UserProfileHeader

import _OneSignal from './OneSignal'
export const OneSignal = _OneSignal

import _CloseButton from './CloseButton'
export const CloseButton = _CloseButton

import _ParaSwiper from './ParaSwiper'
export const ParaSwiper = _ParaSwiper

import _DetailHeader from './Detail/Header'
import _DetailAuthor from './Detail/Author'
import _DetailBack from './Detail/Back'
import _DetailFooter from './Detail/Footer'

export const DetailHeader = _DetailHeader
export const DetailAuthor = _DetailAuthor
export const DetailBack = _DetailBack
export const DetailFooter = _DetailFooter

import _ChatToolBar from './Chat/ToolBar'
export const ChatToolBar = _ChatToolBar

import _SearchChat from './Chat/Search'
export const ChatSearch = _SearchChat

import _StepIndicator from './StepIndicator'
export const StepIndicator = _StepIndicator

import _PaypalPanel from './PaypalPanel'
export const PaypalPanel = _PaypalPanel

import _ProductItem from './ProductItem'
export const ProductItem = _ProductItem

import _CartButton from './CartButton'
export const CartButton = _CartButton

import _SelectImage from './SelectImage'
export const SelectImage = _SelectImage

import _SelectListingTypes from './SelectListingTypes'
export const SelectListingTypes = _SelectListingTypes

import _TypeModal from './TypeModal'
export const TypeModal = _TypeModal

import _HeaderPage from './HeaderPage'
export const HeaderPage = _HeaderPage

import _PostHeading from './PostHeading'
export const PostHeading = _PostHeading

import _ProcessModal from './ProcessModal'
export const ProcessModal = _ProcessModal

import _PostNewsDialog from './PostNewsDialog'
export const PostNewsDialog = _PostNewsDialog

import _PickMap from './MapView/Pick'
export const PickMap = _PickMap

import _SelectCategory from './SelectCategory'
export const SelectCategory = _SelectCategory

import _ListingDataDetail from './Detail/ListingData'
export const ListingDataDetail = _ListingDataDetail

import _TabBarEx from './TabBarEx'
export const TabBarEx = _TabBarEx

import _SearchGooglePlace from './SearchGooglePlace'
export const SearchGooglePlace = _SearchGooglePlace

import _TouchableSacle from './TouchableScale'
export const TouchableScale = _TouchableSacle

import _SlideItem from './SlideItem'
export const SlideItem = _SlideItem

import _BannerImage from './BannerImage'
export const BannerImage = _BannerImage

import _TextHighlight from './TextHighlight'
export const TextHighlight = _TextHighlight
